<?php


namespace App\Controller;


class inscription
{
    public function inscription()
    { ?>

        <!DOCTYPE html>
        <html lang="fr">

        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Inscription</title>
        </head>

        <body>
            <div>
                <div class="header">
                    <a href="accueil.php">Accueil</a>
                    <a href="forum.php">Forum</a>
                    <a href="connexion.php">Connexion</a>
                    <a href="inscription.php">Inscription</a>
                </div>
                <div>
                    <form action="" method="post">
                        <input type="text" placeholder="Email" name="email">
                        <input type="password" placeholder="Password" name="password">
                        <input type="password" placeholder="Confirmation password" name="password-conf">
                        <input type="submit" value="Valider" name="valider">
                    </form>
                </div>
            </div>
        </body>

        </html>

<?php
        die();
    }
}
